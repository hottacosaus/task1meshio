# Task 1
import meshio

msh = meshio.read("simple.msh")

points = msh.points # mesh points 
cells = msh.cells # mesh cells

msh_simpler = meshio.read("simple_mesh.msh")

# Print points
print("Points:")
print(points)

# Inspect cells
for i, cell_block in enumerate(cells):
    cell_type = cell_block.type
    cell_data = cell_block.data
    print(f"\nCell block {i} - Type: {cell_type}")
    print("Cells data:")
    for j, cell in enumerate(cell_data):
        print(f"\tCell {j} points: {cell}")

# Read the simpler mesh and inspect it
simpler_mesh_name = "simpler_mesh.msh"  # Replace with the actual simpler mesh file name
simpler_msh = meshio.read(simpler_mesh_name)

# Extract points and cells for the simpler mesh
simpler_points = simpler_msh.points
simpler_cells = simpler_msh.cells

# Print points of the simpler mesh
print("\nSimpler Mesh Points:")
print(simpler_points)

# Inspect cells of the simpler mesh
for i, cell_block in enumerate(simpler_cells):
    cell_type = cell_block.type
    cell_data = cell_block.data
    print(f"\nSimpler Mesh Cell block {i} - Type: {cell_type}")
    print("Cells data:")
    for j, cell in enumerate(cell_data):
        print(f"\tCell {j} points: {cell}")
